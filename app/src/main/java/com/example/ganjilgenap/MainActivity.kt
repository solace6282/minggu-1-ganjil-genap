package com.example.ganjilgenap

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TableRow
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var op1 = 0
    var op2 = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        submit.setOnClickListener {
            setOp()
            output()
        }

    }

    fun setOp(){
        op1 = op1text.text.toString().toInt()
        op2 = op2text.text.toString().toInt()
    }

    fun output(){
        tableLayout.removeAllViews()
        for (i in op1..op2){
            var newrow = TableRow(this)
            var resText = TextView(this)


            if (i % 2 == 0){
                resText.text = "Angka " + i.toString() + " adalah genap"
            }
            else {
                resText.text = "Angka " + i.toString() + " adalah ganjil"
            }

            newrow.addView(resText)
            tableLayout.addView(newrow)
        }
    }
}